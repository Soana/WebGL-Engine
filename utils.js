var Utils = {
	degToRad: function(deg){
		return (deg/360)*(2*Math.PI);
	},
	radToDeg: function(rad){
		return rad/(2*Math.PI)*360;
	},
	makeElementPointerLockable: function(element, moveTracker, lockOnClick, clickListener){
		element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;
		document.exitPointerLock = document.exitPointerLock || document.mozExitPointerLock || document.webkitExitPointerLock;

		var lockChangeAlert = function(){
			if(document.pointerLockElement === element || document.mozPointerLockElement === element || document.webkitPointerLockElement === element) {
				console.log('The pointer lock status is now locked to ' + element);
				document.onmousemove = moveTracker;
				element.onclick = clickListener;
			} else {
				console.log('The pointer lock status is now unlocked from ' + element);
				document.onmousemove = undefined;
				if(lockOnClick === true){
					element.onclick = function() {
						element.requestPointerLock();
					}
				}
			}

		}.bind(this);
		// Hook pointer lock state change events for different browsers
		document.addEventListener('pointerlockchange', lockChangeAlert, false);
		document.addEventListener('mozpointerlockchange', lockChangeAlert, false);
		document.addEventListener('webkitpointerlockchange', lockChangeAlert, false);

		if(lockOnClick === true){
			element.onclick = function() {
				element.requestPointerLock();
			}
		}
	}
};
