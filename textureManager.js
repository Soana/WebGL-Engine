var TextureManager = function(textures, callback){
	this.textures = {};
	if(textures !== undefined){
		this.loadTextures(textures, callback);
	}
}

TextureManager.prototype = {
	getTexture: function(imageName){
		var image = this.textures[imageName];

		if(image === undefined){
			throw "This image (" + imageName + ") will never be available!";
		}

		if(image.texture === undefined){
			image.texture = this.loadImageToTexture(image);
		}
		return image.texture;
	},

	loadTextures: function(textures, callback){
		var remaining = textures.length;
		for(var i in textures){
			this.textures[textures[i]] = new Image();
			this.textures[textures[i]].src = textures[i];
			this.textures[textures[i]].onload = function(){
				remaining--;
				if(remaining === 0){
					callback();
				}
			}
		}
	},

	loadImageToTexture: function(image){
		var texture = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
		gl.bindTexture(gl.TEXTURE_2D, null);
		return texture;
	}
}
