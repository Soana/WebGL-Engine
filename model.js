var Model = function(model){
	this.load(model);
}

Model.prototype = {
	load: function(model){
		var vertexBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(model.vertices), gl.STATIC_DRAW);

		var indexBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(model.indices), gl.STATIC_DRAW);

		var textureMapBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, textureMapBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(model.textureMap), gl.STATIC_DRAW);

		var normalBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(model.normals), gl.STATIC_DRAW);

		this.buffers =  {
			length: model.indices.length,
			vertices: vertexBuffer,
			indices: indexBuffer,
			normals: normalBuffer,
			textureMap: textureMapBuffer
		};
	},

	getIndexBuffer: function(){
		return this.buffers.indices;
	},

	getIndexBufferLength: function(){
		return this.buffers.length;
	},

	getVertexBuffer: function(){
		return this.buffers.vertices;
	},

	hasNormals: function(){
		return this.buffers.normals !== undefined;
	},

	hasTextureMap: function(){
		return this.buffers.textureMap !== undefined;
	},

	getTextureMapBuffer: function(){
		return this.buffers.textureMap;
	},

}
