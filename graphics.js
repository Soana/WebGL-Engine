var gl;
var Graphics = function(){
	this.projectionMatrix = mat4.create();
	this.viewMatrix = mat4.create();
	this.viewProjectionMatrix = mat4.create();
	this.modelViewProjectionMatrix = mat4.create();
	this.modelMatrix = mat4.create();

	this.sceneShader;

	this.models = {};

	this.entities = [];
	this.playerEntity = new Entity({
		position: [0, 0, 0],
		rotation: [0, 0, 0],
		relativeMoves: true
	});

	this.modelManager = new ModelManager();
	this.textureManager = new TextureManager();

	this.running = false;
	window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
}

Graphics.prototype = {
	initGL: function(_gl, width, height, fieldOfView){
		gl = _gl;

		mat4.perspective(this.projectionMatrix, Utils.degToRad(fieldOfView), width/height, 0.1, 200);

		gl.clearColor(0.7, 0.7, 1.0, 1.0); //himmelblau

		gl.viewport(0, 0, width, height);
		gl.enable(gl.DEPTH_TEST);
		gl.enable(gl.BLEND);
		gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

	},

	start: function(fps){
		this.running = true;
		if(fps === undefined){
			fps = 60;
		}
		if(!window.requestAnimationFrame){
			console.warn("window.requestAnimationFrame not available. Using setTimeout. This might cause performance issues.")
		}
		redraw = function(){
			if(this.running){
				var time = Date.now();
				this.draw();
				var took = Date.now() - time;

				var waittime = 1000/fps-took;
				if(waittime < 0){
					console.warn("Rendering is too much work! (" + took + "/" + (1000/fps) + ")");
				}

				if(window.requestAnimationFrame){
					window.requestAnimationFrame(redraw);
				}
				else{
					setTimeout(redraw, waittime);
				}
			}
		}.bind(this);

		redraw();
	},

	stop: function(){
		this.running = false;
	},

	loadModels: function(models){
		this.modelManager.loadModels(models);
	},

	loadTextures: function(textures, callback){
		this.textureManager.loadTextures(textures, callback);
	},

	draw: function(){
		gl.clear(gl.COLOR_BUFFER_BIT);
		gl.useProgram(this.sceneShader);

		mat4.identity(this.viewMatrix);

		mat4.rotateX(this.viewMatrix, this.viewMatrix, -this.playerEntity.rotation.x());
		mat4.rotateY(this.viewMatrix, this.viewMatrix, -this.playerEntity.rotation.y());
		mat4.rotateZ(this.viewMatrix, this.viewMatrix, -this.playerEntity.rotation.z());
		this.playerEntity.setViewMatrix(this.viewMatrix);
		mat4.translate(this.viewMatrix, this.viewMatrix, [-this.playerEntity.position.x(), -this.playerEntity.position.y(), this.playerEntity.position.z()]);

		mat4.identity(this.viewProjectionMatrix);
		mat4.multiply(this.viewProjectionMatrix, this.projectionMatrix, this.viewMatrix);

		for(var i in this.entities){
			this.drawEntity(this.entities[i]);
		}
	},

	drawEntity: function(entity){
		if(! entity.hasModel()){
			return;
		}

		mat4.identity(this.modelMatrix);

		mat4.translate(this.modelMatrix, this.modelMatrix, [entity.position.x(), entity.position.y(), entity.position.z()]);

		mat4.rotateX(this.modelMatrix, this.modelMatrix, entity.rotation.x());
		mat4.rotateY(this.modelMatrix, this.modelMatrix, entity.rotation.y());
		mat4.rotateZ(this.modelMatrix, this.modelMatrix, entity.rotation.z());

		mat4.multiply(this.modelViewProjectionMatrix, this.viewProjectionMatrix, this.modelMatrix);

		gl.uniformMatrix4fv(this.sceneShader.uModelViewProjectionMatrix, false, this.modelViewProjectionMatrix);

		if(entity.hasTexture()){
			gl.enableVertexAttribArray(this.sceneShader.aTextureMap);
			gl.uniform1i(this.sceneShader.uHasTexture, 1);

			gl.bindBuffer(gl.ARRAY_BUFFER, entity.model.getTextureMapBuffer());
			gl.vertexAttribPointer(this.sceneShader.aTextureMap, 2, gl.FLOAT, false, 0, 0);

			gl.activeTexture(gl.TEXTURE0);
			gl.uniform1i(this.sceneShader.uSampler, 0);
			gl.bindTexture(gl.TEXTURE_2D, this.textureManager.getTexture(entity.texture));
		}
		else{
			gl.uniform1i(this.sceneShader.uHasTexture, 0);
			gl.disableVertexAttribArray(this.sceneShader.aTextureMap);
			if(entity.color !== undefined){
				gl.uniform4fv(this.sceneShader.uColor, entity.color);
			}
			else{
				gl.uniform4fv(this.sceneShader.uColor, [0,0,1,1]);
			}
		}

		gl.bindBuffer(gl.ARRAY_BUFFER, entity.model.getVertexBuffer());
		gl.vertexAttribPointer(this.sceneShader.aPosition, 3, gl.FLOAT, false, 0, 0);

		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, entity.model.getIndexBuffer());

		gl.drawElements(gl.TRIANGLES, entity.model.getIndexBufferLength(), gl.UNSIGNED_SHORT, 0);
	},

	loadModel: function(model){
		var vertexBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(model.vertices), gl.STATIC_DRAW);

		var indexBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(model.indices), gl.STATIC_DRAW);

		return {
			length: model.indices.length,
			vertices: vertexBuffer,
			indices: indexBuffer
		};
	},

	compileShader: function(code, type){
		var shader = gl.createShader(type);
		gl.shaderSource(shader, code);
		gl.compileShader(shader);

		if(! gl.getShaderParameter(shader, gl.COMPILE_STATUS)){
			console.error(gl.getShaderInfoLog(shader));
		}
		else{
			console.log("Successfully compiled shader!");
		}
		return shader;
	},

	initShaders: function(vertShader, fragShader){
		this.sceneShader = gl.createProgram();

		gl.attachShader(this.sceneShader, this.compileShader(vertShader, gl.VERTEX_SHADER));
		gl.attachShader(this.sceneShader, this.compileShader(fragShader, gl.FRAGMENT_SHADER));

		gl.linkProgram(this.sceneShader);

		if(! gl.getProgramParameter(this.sceneShader, gl.LINK_STATUS)){
			console.error("Could not link shaders!");
		}
		else{
			console.log("Successfully linked shaders!");
			this.findShaderParameters();
		}
	},

	findShaderParameters: function(){
		this.sceneShader.aTextureMap = gl.getAttribLocation(this.sceneShader, "aTextureMap");
		this.sceneShader.aPosition = gl.getAttribLocation(this.sceneShader, "aPosition");
		this.sceneShader.uModelViewProjectionMatrix = gl.getUniformLocation(this.sceneShader, "uModelViewProjectionMatrix");

		this.sceneShader.uHasTexture = gl.getUniformLocation(this.sceneShader, "uHasTexture");
		this.sceneShader.uSampler = gl.getUniformLocation(this.sceneShader, "uSampler");
		this.sceneShader.uColor = gl.getUniformLocation(this.sceneShader, "uColor");

		gl.enableVertexAttribArray(this.sceneShader.aPosition);
		gl.enableVertexAttribArray(this.sceneShader.aTextureMap);
	},

	attachEntity: function(entity){
		this.entities.push(entity);
	},

	removeEntity: function(entity){
		var index = this.entities.indexOf(entity);
		if(index === -1){
			throw "This entity is not attached to this Graphics!";
		}
		else{
			this.entites.splice(index, 1);
		}
	},

	setPlayerPosition: function(position){
		this.playerEntity.setPosition(position);
	},

	setPlayerRotation: function(rotation){
		this.playerEntity.setRotation(rotation);
	}
}
