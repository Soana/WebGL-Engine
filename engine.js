var Engine = function(){
	this.graphics = new Graphics();
	this.ticToc = new TicToc();
}

Engine.prototype = {
	speed: 0.02,
	init: function(args, callback){
		if(args.tick !== undefined){
			this.playerAction = {};
			if(args.tick.moveable !== undefined && args.tick.moveable === true){
				this.playerAction.moving = {
					forwards: false,
					backwards: false,
					left: false,
					right: false
				};
				var moveDetected = function(key, moving){
					var moveKey = false;
					if(key === 87 || key === 38){ // W and arrow up
						this.playerAction.moving.forwards = moving;
						moveKey = true;
					}
					else if(key === 65 || key === 37){ //A and arrow left
						this.playerAction.moving.left = moving;
						moveKey = true;
					}
					else if(key === 83 || key === 40){ //S and arrow down
						this.playerAction.moving.backwards = moving;
						moveKey = true;
					}
					else if(key === 68 || key === 39){ //D and arrow right
						this.playerAction.moving.right = moving;
						moveKey = true;
					}
					return moveKey;
				}.bind(this);

				document.onkeydown = function(event){
					if(moveDetected(event.keyCode, true)){
						event.preventDefault();
					}
				}
				document.onkeyup = function(event){
					if(moveDetected(event.keyCode, false)){
						event.preventDefault();
					}
				}

				this.ticToc.attach(function(){
					var movement = [0,0,0];
					var moving = false;
					if(this.playerAction.moving.forwards === true){
						movement[2] -= this.speed;
						moving = true;
					}
					if(this.playerAction.moving.backwards === true){
						movement[2] += this.speed;
						moving = true;
					}
					if(this.playerAction.moving.left === true){
						movement[0] -= this.speed;
						moving = true;
					}
					if(this.playerAction.moving.right === true){
						movement[0] += this.speed;
						moving = true;
					}

					if(moving){
						this.graphics.playerEntity.moveRelative(new Vector(movement), this.speed);
					}
				}.bind(this));
			}
			if(args.tick.rotateable !== undefined && args.tick.rotateable === true){
				this.playerAction.rotation = {};

				Utils.makeElementPointerLockable(args.canvas, function(event){
					this.playerAction.rotation.current = {
						x: - (event.movementX || event.mozMovementX || event.webkitMovementX || 0),
						y: - (event.movementY || event.mozMovementY || event.webkitMovementY || 0)
					};
				}.bind(this), true, function(){
					console.log("rotation: " + this.graphics.playerEntity.rotation.values);
					console.log("position: " + this.graphics.playerEntity.position.values);
				}.bind(this));

				var radFieldOfView = Utils.degToRad(args.graphics.fieldOfView);
				this.ticToc.attach(function(){
					var rotation = [0,0,0];
					var current = this.playerAction.rotation.current;
					if(current !== undefined){
						rotation[0] = current.y/args.canvas.height * 2*Math.PI;
						rotation[1] = current.x/args.canvas.width * 2*Math.PI;
						this.graphics.playerEntity.rotate(new Vector(rotation));
						if(this.graphics.playerEntity.rotation.x() > Math.PI/2){
							this.graphics.playerEntity.rotation.values[0] = Math.PI/2;
						}
						else if(this.graphics.playerEntity.rotation.x() < -Math.PI/2){
							this.graphics.playerEntity.rotation.values[0] = -Math.PI/2;
						}
						this.playerAction.rotation.current = undefined;
					}
				}.bind(this));
			}
		}

		this.graphics.initGL(args.graphics.gl, args.graphics.width, args.graphics.height, args.graphics.fieldOfView);
		this.graphics.initShaders(args.graphics.shader.vertShader, args.graphics.shader.fragShader);
		if(args.graphics.models !== undefined){
			this.loadModels(args.graphics.models);
		}
		if(args.graphics.textures !== undefined){
			this.loadTextures(args.graphics.textures, callback);
		}
		else{
			callback();
		}
	},

	attach: function(entity){
		this.graphics.attachEntity(entity);
		if(entity.isTickable()){
			this.ticToc.attach(entity.tick);
		}
		entity.attach(this);
	},

	remove: function(entity){
		this.graphics.removeEntity(entity);
		if(entity.isTickable()){
			this.ticToc.remove(entity);
		}
		entity.detach(this);
	},

	loadModels: function(models){
		this.graphics.loadModels(models);
	},

	loadTextures: function(models, callback){
		this.graphics.loadTextures(models, callback);
	},

	start: function(ps){
		if(ps === undefined){
			ps = {};
		}
		if(ps.fps !== undefined){
			this.graphics.start(ps.fps);
		}
		else if(ps.anyps !== undefined){
			this.graphics.start(ps.anyps);
		}
		else{
			this.graphics.start();
		}

		if(ps.tps !== undefined){
			this.ticToc.start(ps.tps);
		}
		else if(ps.anyps !== undefined){
			this.ticToc.start(ps.anyps);
		}
		else{
			this.ticToc.start();
		}
	},

	stop: function(){
		this.graphics.stop();
		this.ticToc.stop();
	},

	getModelManager: function(){
		return this.graphics.modelManager;
	},

	setPlayerPosition: function(position){
		this.graphics.setPlayerPosition(position);
	},

	setPlayerRotation: function(rotation){
		this.graphics.setPlayerRotation(rotation);
	}
}
