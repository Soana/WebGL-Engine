var ModelManager = function(models){
	this.models = {};
	if(models !== undefined){
		this.loadModels(models);
	}
}

ModelManager.prototype = {
	getModel: function(name){
		if(this.models[name] === undefined){
			throw "Invalid model: " + name;
		}
		else{
			return this.models[name];
		}
	},

	loadModels: function(models){
		for(var i in models){
			this.models[models[i].name] = new Model(models[i]);
		}
	}
}
