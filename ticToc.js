var TicToc = function(){
	this.callbacks = [];
	this.running = false;
}

TicToc.prototype = {
	attach: function(callback){
		this.callbacks.push(callback);
	},

	remove: function(callback){
		var index = this.callbacks.indexOf(callback);
		if(index === -1){
			throw "Trying to remove unregistered callback!";
		}
		else{
			this.callbacks.splice(index, 1);
		}
	},

	start: function(tps){
		this.running = true;
		this.currentTick = 0;
		if(tps === undefined){
			tps = 60;
		}

		tick = function(){
			if(this.running){
				var time = Date.now();
				for(var i in this.callbacks){
					this.callbacks[i](this.currentTick);
				}
				this.currentTick++;
				var took = Date.now() - time;

				var waittime = 1000/tps-took;
				if(waittime < 0){
					console.warn("Ticking is too much work! (" + took + "/" + (1000/tps) + ")");
					waittime = 0;
				}
				setTimeout(tick, waittime);
			}
		}.bind(this);

		tick();
	},

	stop: function(){
		this.running = false;
	}
}
