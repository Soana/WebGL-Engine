var Entity = function(args){
	if(args === undefined){
		throw "Must have arguments {position=[0,0,0], rotation=[0,0,0], (model), (texture|color), (tick)}!";
	}
	if(args.position instanceof Array){
		args.position = new Vector(args.position);
	}
	this.position = args.position === undefined ? new Vector(0, 0, 0): args.position;

	if(args.rotation instanceof Array){
		args.rotation = new Vector(args.rotation);
	}
	this.rotation = args.rotation === undefined ? new Vector(0, 0, 0): args.rotation;

	this.model = args.model;
	this.texture = args.texture;
	this.color = args.color;

	this.makeTickable(args.tick);

	if(args.engine !== undefined){
		this.attach(args.engine);
	}

	if(args.relativeMoves !== undefined && args.relativeMoves === true){
		this.relativeMove = {
			viewMatrix : mat4.create(),
			undirectedMove: vec4.create(),
			directedMove: vec4.create()
		};
	}
}

Entity.prototype = {
	rotate: function(rotation){
		this.rotation = this.rotation.add(rotation).trim(2*Math.PI);
	},

	moveRelative: function(move, speed){
		if(this.relativeMove !== undefined){
			var normalizedMove = move.normalized().mult(speed);
			console.log(normalizedMove);
			vec4.set(this.relativeMove.undirectedMove, normalizedMove.x(), normalizedMove.y(), normalizedMove.z(), 1);
			vec4.transformMat4(this.relativeMove.directedMove, this.relativeMove.undirectedMove, this.relativeMove.viewMatrix);
			var directedMove = this.relativeMove.directedMove;
			this.position = this.position.add(new Vector(directedMove[0], directedMove[1], directedMove[2]));
			console.log(mat4.str(this.relativeMove.viewMatrix) + " * " + vec4.str(this.relativeMove.undirectedMove) + " = " + vec4.str(this.relativeMove.directedMove) + " --> " + this.position.values);
		}
		else{
			throw "This entity cannot be moved relatively to its rotation!";
		}
			// var cos = [Math.cos(this.rotation.x()), Math.cos(this.rotation.y()), Math.cos(this.rotation.z())];
			// var sin = [Math.sin(this.rotation.x()), Math.sin(this.rotation.y()), Math.sin(this.rotation.z())];
			// var directionalMove = move.multMat([[cos[1], 0, -sin[1]], [0, 1, 0], [sin[1], 0, cos[1]]])
			// 							.multMat([[1, 0, 0], [0, cos[0], sin[0]], [0, -sin[0], cos[0]]])
			// 							.multMat([[cos[2], sin[2], 0], [-sin[2], cos[2], 0], [0, 0, 1]]);
			// this.position = this.position.add(directionalMove);
			// console.log("[" + move.values + "] -> [" + directionalMove.values + "] (" + this.rotation.values + "]) = [" + this.position.values + "]");
	},
	setViewMatrix: function(viewMatrix){
		if(this.relativeMove !== undefined){
			mat4.copy(this.relativeMove.viewMatrix, viewMatrix);
		}
		else{
			throw "Relative movement is not enabled for this entity!";
		}
	},

	moveAbsolute: function(move){
		this.position = this.position.add(move);
	},

	hasModel: function(){
		return this.model !== undefined;
	},

	attach: function(enginge){
		this.engine = engine;
	},

	detach: function(){
		if(this.engine !== undefined){
			this.engine = undefined;
		}
		else{
			throw "This entity is not attached to any Engine!";
		}
	},

	isTickable: function(){
		return this.tick !== undefined;
	},

	makeTickable: function(ticker){
		if(ticker !== undefined){
			this.tick = ticker.bind(this);
		}
	},

	isAttached: function(){
		return this.graphics !== undefined;
	},

	hasTexture: function(){
		return this.texture !== undefined;
	},

	getSample: function(){
		return 7;
	},

	setPosition: function(position){
		if(position instanceof Array){
			position = new Vector(position);
		}
		else if(arguments.length > 1){
			position = new Vector(arguments);
		}
		this.position = position;
	},

	setRotation: function(rotation){
		if(rotation instanceof Array){
			rotation = new Vector(rotation);
		}
		else if(arguments.length > 1){
			rotation = new Vector(arguments);
		}
		this.rotation = rotation;
	}
}
