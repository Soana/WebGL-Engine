precision lowp float;

attribute vec3 aPosition;

uniform mat4 uViewProjectionMatrix;

void main(void){

	gl_Position = uViewProjectionMatrix * vec4(aPosition, 1.0);
}
