var Vector = function(values){
	if(arguments.length > 1){
		this.values = arguments;
	}
	else{
		this.values = values;
	}
	this.dimension = this.values.length;
	this.length = Math.sqrt(this.prod(this));
}

Vector.prototype = {
	get: function(index){
		if(this.dimension <= index){
			throw "Illegal index: " + index  + " >= " + this.dimension;
		}
		return this.values[index];
	},
	x: function(){
		return this.get(0);
	},
	y: function(){
		return this.get(1);
	},
	z: function(){
		return this.get(2);
	},
	w: function(){
		return this.get(3);
	},
	add: function(vector){
		if(this.dimension !== vector.dimension){
			throw "Only vectors of the same dimension can be added! " + this.dimension + " != " + vector.dimension;
		}
		var values = [];
		for(var i = 0; i < this.dimension; i++){
			values.push(this.values[i] + vector.values[i]);
		}
		return new Vector(values);
	},
	sub: function(vector){
		if(this.dimension !== vector.dimension){
			throw "Only vectors of the same dimension can be substracted! " + this.dimension + " != " + vector.dimension;
		}
		return this.add(vector.mult(-1));
	},
	mult: function(scalar){
		var values = [];
		for(var i = 0; i < this.dimension; i++){
			values.push(this.values[i] * scalar);
		}
		return new Vector(values);
	},
	multMat: function(matrix){
		if(matrix[0].length !== this.dimension){
			throw "Multiplication of A(" + matrix.length + "x" + matrix[0].length + ") * x(" + this.dimension + "x1) is not possible!";
		}
		else{
			var values = new Array(matrix.length);
			for(var i = 0; i < matrix.length; i++){
				values[i] = 0;
				for(var j = 0; j < this.dimension; j++){
					values[i] += this.values[j] * matrix[i][j];
				}
			}
		}
		return new Vector(values);
	},
	normalized: function(){
		var values = [];
		for(var i = 0; i < this.dimension; i++){
			values.push(this.values[i]/this.length);
		}
		return new Vector(values);
	},
	prod: function(vector){
		if(this.dimension !== vector.dimension){
			throw "The scalar product can only be calculated from vectors of the same dimension! " + this.dimension + " != " + vector.dimension;
		}
		var prod = 0;
		for(var i = 0; i < this.dimension; i++){
			prod += this.values[i] * vector.values[i];
		}
		return prod;
	},
	angleBetween: function(vector){
		if(this.dimension !== vector.dimension){
			throw "The angle between vectors can only be calculated from vectors of the same dimension! " + this.dimension + " != " + vector.dimension;
		}
		return Math.acos(this.prod(vector)/(this.length * vector.length));
	},
	fromAngle: function(vectors, angle){
		if(vectors[0].dimension === 2){
			var vector = vectors[0];
			return new Vector([vector.length * Math.cos(angle)/vector.values[1], 1]);
		}
		// else if(vectors[0].dimension === 3){
		// 	angle = angle %(Math.PI/2);
		//
		// }
		else{
			throw "Creating a vector from an angle is only supported for 2D! (not " + vectors[0].length + "D)";
		}

	},
	trim: function(to){
		var values = this.values;
		for(var i = 0; i < this.dimension; i++){
			values[i] = values[i]%to;
		}
		return new Vector(values);
	}
}
